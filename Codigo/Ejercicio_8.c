#include <stdio.h>
#include <stdlib.h>

int main() {
    int dimension;
    
    printf("Ingrese la dimension de los vectores: ");
    scanf("%d", &dimension);
    
    // Asignar memoria dinámica para los vectores
    double *vector1 = (double *)malloc(dimension * sizeof(double));
    double *vector2 = (double *)malloc(dimension * sizeof(double));
    
    if (vector1 == NULL || vector2 == NULL) {
        printf("Error al asignar memoria dinamica.\n");
        return 1;
    }
    
    // Pedir al usuario los valores para el primer vector
    printf("Ingrese los valores del primer vector:\n");
    for (int i = 0; i < dimension; i++) {
        printf("Elemento %d: ", i + 1);
        scanf("%lf", &vector1[i]);
    }
    
    // Pedir al usuario los valores para el segundo vector
    printf("Ingrese los valores del segundo vector:\n");
    for (int i = 0; i < dimension; i++) {
        printf("Elemento %d: ", i + 1);
        scanf("%lf", &vector2[i]);
    }
    
    // Calcular el producto escalar
    double producto_escalar = 0.0;
    for (int i = 0; i < dimension; i++) {
        producto_escalar += vector1[i] * vector2[i];
    }
    
    // Imprimir el resultado del producto escalar
    printf("El producto escalar de los dos vectores es: %lf\n", producto_escalar);
    
    // Liberar la memoria asignada a los vectores
    free(vector1);
    free(vector2);
    
    return 0;
}
