#include <stdio.h>

typedef struct {
    double x;
    double y;
    double z;
    double x_1;
    double y_1;
    double z_1;
} Vector;

typedef struct {
    double data[3][3];
} Matrix;

void vectorSubtraction(Vector *result, Vector *v1, Vector *v2) {
    result->x = v1->x - (v2->x)*3;
    result->x_1 = v1->x - v2->x;
    result->y = v1->y - (v2->y)*3;
    result->y_1 = v1->y - v2->y;
    result->z = v1->z - (v2->z)*3;
    result->z_1 = v1->z - v2->z;
}

void matrixVectorMultiplication(Vector *result, Matrix *matrix, Vector *vector) {
    result->x = matrix->data[0][0] * vector->x + matrix->data[0][1] * vector->y + matrix->data[0][2] * vector->z;
    result->y = matrix->data[1][0] * vector->x + matrix->data[1][1] * vector->y + matrix->data[1][2] * vector->z;
    result->z = matrix->data[2][0] * vector->x + matrix->data[2][1] * vector->y + matrix->data[2][2] * vector->z;
}

void matrixScalarMultiplication(Matrix *result, Matrix *matrix, double scalar) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            result->data[i][j] = scalar * matrix->data[i][j];
        }
    }
}

void matrixMatrixMultiplication(Matrix *result, Matrix *matrix1, Matrix *matrix2) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            result->data[i][j] = 0.0;
            for (int k = 0; k < 3; k++) {
                result->data[i][j] += matrix1->data[i][k] * matrix2->data[k][j];
            }
        }
    }
}

int main() {
    // Definir los vectores u y v
    Vector u = {1, 2, 3};
    Vector v = {6, 5, 4};

    // Definir las matrices A y B
    Matrix A = {.data = {{1, 5, 0}, {7, 1, 2}, {0, 0, 1}}};
    Matrix B = {.data = {{-2, 0, 1}, {1, 0, 0}, {4, 1, 0}}};

    // 1. w = u - 3v
    Vector w;
    vectorSubtraction(&w, &u, &v);
    printf("w = (%lf, %lf, %lf)\n", w.x, w.y, w.z);

    // 2. x = u - v
    Vector x;
    vectorSubtraction(&x, &u, &v);
    printf("x = (%lf, %lf, %lf)\n", x.x_1, x.y_1, x.z_1);

    // 3. y = Au
    Vector y;
    matrixVectorMultiplication(&y, &A, &u);
    printf("y = (%lf, %lf, %lf)\n", y.x, y.y, y.z);

    // 4. z = Au - v
    Vector z;
    Vector Au;
    matrixVectorMultiplication(&Au, &A, &u);
    vectorSubtraction(&z, &Au, &v);
    printf("z = (%lf, %lf, %lf)\n", z.x_1, z.y_1, z.z_1);

    // 5. C = 4A - 3B
    Matrix C;
    Matrix tempA, tempB;
    matrixScalarMultiplication(&tempA, &A, 4);
    matrixScalarMultiplication(&tempB, &B, 3);
    matrixMatrixMultiplication(&C, &tempA, &tempB);
    printf("C =\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%lf ", C.data[i][j]);
        }
        printf("\n");
    }

    // 6. D = AB
    Matrix D;
    matrixMatrixMultiplication(&D, &A, &B);
    printf("D =\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%lf ", D.data[i][j]);
        }
        printf("\n");
    }

    return 0;
}
