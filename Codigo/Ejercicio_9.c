#include <stdio.h>

int calcularsumaYproducto(double *suma, double *producto, double x, double y){

    *suma= x + y;

    *producto = x * y;

    return 0;
}

int main() {

    double x = 0.0;
    double y = 0.0;
    double sum, producto;
    
    printf("Ingresa dos valores que quieras sumar y multiplicar: \n");
    scanf("%le", &x);
    scanf("%le", &y);

    int result=calcularsumaYproducto(&sum, &producto, x, y);
    
    if (result == 0){
        printf("La suma de %.2f y %.2f es: %.2f \n", x, y, sum);
        printf("El producto de %.2f y %.2f es: %.2f \n", x, y, producto);

    } else{
        printf("Error al calcular la suma y el producto. \n");
    }

    return 0;
}
