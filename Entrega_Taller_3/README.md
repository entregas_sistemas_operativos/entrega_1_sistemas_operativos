# Entrega Taller LeetCode

Entrega de actividad en clase, relacionada al desarrollo de puntos en leetCode.

## Punto Uno: TwoSum

### Solución a

El siguiente codigo crea un array el cual tendria los resultados entre la posición 0 y 1, seguido usamos for anidados
para recorrer lo suministrado por la plataforma y validamos con un if si el resultado de la suma de ambos es
igual al target, de ser así los asignamos a los indices 0 y 1 de del array de resultados y lo retornamos.

```C
int* twoSum(int* nums, int numsSize, int target, int* returnSize){
    int *retarr=malloc(2*sizeof(int));
    retarr[0]=1;
    retarr[1]=1;
    for(int i=0; i<numsSize;++i){
        for(int j=i+1;j<numsSize; ++j){
            if(nums[i]+nums[j]==target){
                *returnSize=2;
                retarr[0]=i;
                retarr[1]=j;
                return retarr;
            }
        }
    }
    *returnSize=0;
    return NULL;
}
```

**Nota:**

- T.Ejecución: 76 ms
- Memoria: 6.38 mb

### Solución b

En el siguiente codigo hacemos uso de la funcion malloc() debido a que esta
requiere dos atributos (el número de variables para asignar en la memoria "Key o clave"
y el tamaño en bytes de una sola variable "Variable o Valor"),
las hashTable donde refHash seria la clave, finder seria la variable o valor de
la clave y num seria el numero que se compararia de manera indetermiada hasta
encontrar la solución y por ultimo los procesos de validación serian como los del punto anterior

```C
typedef struct {
	int num;
	int idx;
	UT_hash_handle hh;
} num_t;

int* twoSum(int* nums, int numsSize, int target, int* returnSize){
	int* res = calloc((*returnSize = 2), sizeof(int));
	num_t *refHash = NULL, *num = NULL, *tmp = NULL;

	for(int i = 0; i < numsSize; ++i){
		int finder = target - nums[i];
		HASH_FIND_INT(refHash, &finder, num);

		if (num != NULL){
			res[0] = num->idx;
			res[1] = i;

			break;
		} else {
			num = malloc(sizeof(num_t));
			num->num = nums[i]; num->idx = i;

			HASH_ADD_INT(refHash, num, num);
		}
	}

	HASH_ITER(hh, refHash, num, tmp) {
		HASH_DEL(refHash, num); free(num);
	}

	return res;
}
```

**Nota:**

- T.Ejecución: 3 ms
- Memoria: 7,72 mb

## Remove Duplicates from Sorted Array

### Solución a

en el siguiente ejercicio se planteo la solucion por medio de la creacion
de matrices de numero enteros en orden decreciente para posteriormente
se eliminaron los duplicados y devolvemos la matriz.

```C
int removeDuplicates(int* nums, int numsSize) {
    if (numsSize <= 1) {
        return numsSize;
    }

    int uniqueIndex = 1;
    int currentElement = nums[0];

    for (int i = 1; i < numsSize; i++) {
        if (nums[i] != currentElement) {
            nums[uniqueIndex] = nums[i];
            currentElement = nums[i];
            uniqueIndex++;
        }
    }

    return uniqueIndex;
}
```

**Nota:**

- T.Ejecución: 15 ms
- Memoria, 7,80 mb

### Solución b

se resuelve el problema de eliminar números repetidos de una lista de números ordenados, manteniendo el orden y contando cuántos números únicos hay. Para hacerlo, utiliza dos apuntadores. Al final, el número de lugares donde está el apuntador lento más uno es la cantidad de números únicos en la lista.

```C
int removeDuplicates(int* nums, int numsSize) {
    if (numsSize <= 1) {
        return numsSize;
    }

    int slow = 0;
    int fast = 1;

    while (fast < numsSize) {
        if (nums[fast] != nums[slow]) {
            slow++;
            nums[slow] = nums[fast];
        }
        fast++;
    }

    return slow + 1;
}
```

**Nota:**

- Tiempo de ejecucion: 14 ms
- Memoria: 7,80 MB

### Ejercicio 3: Remove Element

### Solución a

se realizo una matriz de enteros nums y un entero val, se elimina todas las apariciones de val in nums -place. Se puede cambiar el orden de los elementos. Luego devuelva el número de elementos en nums los que no son iguales a val.

```C
int removeElement(int* nums, int numsSize, int val){
    int size = 0;
    for (int i = 0; i < numsSize; i++) {
        if (nums[i] != val) {
            nums[size++] = nums[i];
        }
    }

    return size;
}
```

**Nota:**

- Tiempo de ejecucion: 2 ms
- Memoria: 6,30 MB

### Solucion b

En el siguiente algoritmo optimizado se utilizó un enfoque simple, donde se recorre la lista, copia los numeros que no son iguales al numero especial en la posicion correspondiente de la lista original.

```C
    int removeElement(int* nums, int numsSize, int val) {
    int size = 0;
    for (int i = 0; i < numsSize; i++) {
        if (nums[i] != val) {
            nums[size] = nums[i];
            size++;
        }
    }

    return size;
}
```

### Ejercicio 4: Median of Two Sorted Arrays

En el siguiente codigo se realizo el recorrido de una lista con la finalidad de hallar la mediana.

```C
double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2, int nums2Size) {
    if (nums1Size > nums2Size) {
        return findMedianSortedArrays(nums2, nums2Size, nums1, nums1Size);
    }

    int x = nums1Size;
    int y = nums2Size;
    int low = 0;
    int high = x;

    while (low <= high) {
        int partitionX = (low + high) / 2;
        int partitionY = (x + y + 1) / 2 - partitionX;

        int maxX = (partitionX == 0) ? INT_MIN : nums1[partitionX - 1];
        int maxY = (partitionY == 0) ? INT_MIN : nums2[partitionY - 1];

        int minX = (partitionX == x) ? INT_MAX : nums1[partitionX];
        int minY = (partitionY == y) ? INT_MAX : nums2[partitionY];

        if (maxX <= minY && maxY <= minX) {
            if ((x + y) % 2 == 0) {
                return ((double) (fmax(maxX, maxY) + fmin(minX, minY))) / 2;
            } else {
                return (double) fmax(maxX, maxY);
            }
        } else if (maxX > minY) {
            high = partitionX - 1;
        } else {
            low = partitionX + 1;
        }
    }
    return -1;
}
```

**Nota:**

- Tiempo de ejecucion: 1 ms
- Memoria: 5,94 MB

# Ejemplo link

-[Link a mi block](https://direl.gitlab.io/blog)
