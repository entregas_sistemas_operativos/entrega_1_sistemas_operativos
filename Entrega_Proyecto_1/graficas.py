import numpy as np
import matplotlib.pyplot as plt

# Leer los valores desde el archivo "respuestas.txt"
with open("resultados.txt", "r") as file:
    lines = file.readlines()

n_values, x_values, psi_values, probability_values, potential_values = [], [], [], [], []

for line in lines:
    parts = line.strip().split(', ')
    n = int(parts[0].split('=')[1])
    x = float(parts[1].split('=')[1])
    psi = float(parts[2].split('=')[1])
    probability = float(parts[3].split('=')[1])
    potential = float(parts[4].split('=')[1])

    n_values.append(n)
    x_values.append(x)
    psi_values.append(psi)
    probability_values.append(probability)
    potential_values.append(potential)

# Crear subplots para graficar
plt.figure(figsize=(12, 8))

for n in range(max(n_values) + 1):
    plt.subplot(2, 2, n + 1)
    indices = [i for i, n_val in enumerate(n_values) if n_val == n]
    plt.plot([x_values[i] for i in indices], [psi_values[i] for i in indices], label=f'n = {n}')
    plt.plot([x_values[i] for i in indices], [probability_values[i] for i in indices], label=f'n = {n} Density')
    plt.plot(x_values, potential_values, label='Potencial', linestyle='--')
    plt.title(f'Función de Onda, Densidad de Probabilidad y Potencial para n = {n}')
    plt.xlabel('x')
    plt.grid(True)
    plt.legend()

plt.tight_layout()
plt.show()
