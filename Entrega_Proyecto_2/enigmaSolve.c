#include <stdio.h>
#include <string.h>

char abecedario[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
char rotor3[] = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";
char rotor2[] = "AJDKSIRUXBLHWTMCQGZNPYFVOE";
char rotor1[] = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
char mensajeCifrado[] =
    "GEUDOPPSJPDDKDBJQBNYXGKKFYKTGMIWIHLGNDMGBEJOMKXICRECGMYHDT";

char buscarCorrespondenciaCapa3(char letra, char *abecedario, char *rotor3) {
  for (int i = 0; rotor3[i] != '\0'; i++) {
    if (rotor3[i] == letra) {
      return abecedario[i];
    }
  }
  return letra; // Si no se encuentra en el rotor3, devolver la letra original
}

char buscarCorrespondenciaCapa2(char letra, char *abecedario, char *rotor2) {
  for (int i = 0; rotor2[i] != '\0'; i++) {
    if (rotor2[i] == letra) {
      return abecedario[i];
    }
  }
  return letra; // Si no se encuentra en el rotor2, devolver la letra original
}

char buscarCorrespondenciaCapa1(char letra, char *abecedario, char *rotor1) {
  for (int i = 0; rotor1[i] != '\0'; i++) {
    if (rotor1[i] == letra) {
      return abecedario[i];
    }
  }
  return letra; // Si no se encuentra en el rotor1, devolver la letra original
}

char descifrar_letra(char letra_cifrada, int posicion) {
  if (letra_cifrada == ' ') {
    return ' '; // Mantener espacios en blanco sin cambios
  }

  for (int i = 0; abecedario[i] != '\0'; i++) {
    if (abecedario[i] == letra_cifrada) {
      // Restar un número fijo de posiciones
      int indice_resultado = (i - posicion + 26) % 26;
      return abecedario[indice_resultado];
    }
  }

  return letra_cifrada; // Si no se encuentra en el abecedario, devolver la
                        // letra original
}

char *descifrarMensaje3(char *mensajeCifrado, char *abecedario, char *rotor3) {
  char letrasDescifradas[1000]; // Ajusta el tamaño de memoria
  int j = 0;

  for (int i = 0; mensajeCifrado[i] != '\0'; i++) {
    char letraCifrada = mensajeCifrado[i];
    if (letraCifrada != ' ') {
      char letraDescifrada =
          buscarCorrespondenciaCapa3(letraCifrada, abecedario, rotor3);
      letrasDescifradas[j] = letraDescifrada;
      j++;
    } else {
      letrasDescifradas[j] = ' ';
      j++;
    }
  }

  letrasDescifradas[j] = '\0'; // Agrega el carácter nulo al final de la cadena
  return strdup(
      letrasDescifradas); // Duplica la cadena para evitar problemas de alcance
}

char *descifrarMensaje2(char *mensajeCifrado, char *abecedario, char *rotor2) {
  char letrasDescifradas[1000];
  int j = 0;
  for (int i = 0; mensajeCifrado[i] != '\0'; i++) {
    char letraCifrada = mensajeCifrado[i];
    if (letraCifrada != ' ') {
      char letraDescifrada =
          buscarCorrespondenciaCapa2(letraCifrada, abecedario, rotor2);
      letrasDescifradas[j] = letraDescifrada;
      j++;
    } else {
      letrasDescifradas[j] = ' ';
      j++;
    }
  }
  letrasDescifradas[j] = '\0';
  return strdup(letrasDescifradas);
}

char *descifrarMensaje1(char *mensajeCifrado, char *abecedario, char *rotor1) {
  char letrasDescifradas[1000];
  int j = 0;
  for (int i = 0; mensajeCifrado[i] != '\0'; i++) {
    char letraCifrada = mensajeCifrado[i];
    if (letraCifrada != ' ') {
      char letraDescifrada =
          buscarCorrespondenciaCapa1(letraCifrada, abecedario, rotor1);
      letrasDescifradas[j] = letraDescifrada;
      j++;
    } else {
      letrasDescifradas[j] = ' ';
      j++;
    }
  }
  letrasDescifradas[j] = '\0';
  return strdup(letrasDescifradas);
}

void laFuncionQueTodoLoPuede42(char *mensaje_cifrado) {
  int longitud = strlen(mensaje_cifrado);
  int posicion = 4; // Posición inicial
  printf("@Direl, esta vez la respuesta a tus problemas no es el 42, la "
         "respuesta es:\n");
  for (int i = 0; i < longitud; i++) {
    char letra_descifrada = descifrar_letra(mensaje_cifrado[i], posicion);
    printf("%c", letra_descifrada);
    posicion++;
  }
  printf("\n");
}

int main() {

  char *letrasDescifradasCapa3 =
      descifrarMensaje3(mensajeCifrado, abecedario, rotor3);
  printf("Letras descifradas por el rotor 3: %s\n", letrasDescifradasCapa3);

  char *letrasDescifradasCapa2 =
      descifrarMensaje2(letrasDescifradasCapa3, abecedario, rotor2);
  printf("Letras descifradas por el rotor 2: %s\n", letrasDescifradasCapa2);

  char *letrasDescifradasCapa1 =
      descifrarMensaje1(letrasDescifradasCapa2, abecedario, rotor1);
  printf("Letras descifradas por el rotor 1: %s\n", letrasDescifradasCapa1);
  // Libera la memoria asignada por strdup

  laFuncionQueTodoLoPuede42(letrasDescifradasCapa1);
  return 0;
}