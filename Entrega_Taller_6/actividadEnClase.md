# <center>Entrega 6 </center>
## <center>Actividad en clase</center>
### <center>Ejercicio</center>
<div style="text-align:justify;">

- Descargue los código del Ejemplo 07, los puede encontrar en el repositorio del docente.

- Compile: `mpicc -Wall -o ejecutable.exe main.c
calculate_pi.c`

- Ejecute: `mpirun -np 2 ./ejecutable.exe 1000`, donde
1000 es N (muestras o puntos).

- Pruebe el código anterior con por lo menos 5 valores de N de
diferente magnitud...

- Compare esté código con el código en python visto previamente
y su código desarrollado en lenguaje C y escriba sus
conclusiones.
</div>

<p style="text-align:justify;">
    Haciendo revisión de los codigos anteriormente suministrados por el docente se puede llegar a las siguinetes conclusiones:
</p>

<div style="text-align:justify;">
    
- La computación en paralelo se refiere a la ejecución simultánea de múltiples tareas o procesos de cómputo, con el objetivo de resolver problemas de manera más rápida y eficiente que utilizando una sola unidad de procesamiento.

- El codigo `main.c` recibe un argumento desde la terminal al momento de ejecutarse, este argumento vendria a ser la cantidad de muestras a generar para hacer una estimación de PI, luego se dividirá el numero de muestras entre el numero de tareas para hacer una distribución del trabajo entre los nucleos del equipo, donde posteriormente se llama a `calculate_pi.c` para hacer todo el procesamiento de la tarea y seguidamente haciendo la impresión de los resultados en la terminal.

- El codigo `calculate_pi.c` vendria a indicar una funcion `create_and_check_coordinates` donde se generan coordenadas `(x,y)` de manera aleatoria en el intervalo `[0,1)` y verificando asi si estan dentro del circulo, luego se define una función do_checks que realiza verificaciones y devuelve la cantidad de pares de coordenadas dentro del círculo, asi mismo define una función `calculate_pi` que utiliza las funciones anteriores para calcular `π` utilizando el método de Montecarlo para un número dado de intentos (cabe resaltar que cada tarea MPI tiene su propio generador de números aleatorios inicializado con la semilla igual al número de tarea y las tareas imprimen información sobre el número de intentos realizados y su número de tarea).Por ultimo se realiza una reducción de todos los resultados de las tareas utilizando MPI_Allreduce para obtener la suma global de intentos dentro del círculo `N_in_global` y el número total de intentos `N_global`.Finalmente, se calcula y devuelve π como `4 * (N_in_global / N_global)`.

</div>

<p style="text-align:justify;">
    En conclusion, la computación en paralelo es una poderosa técnica que permite realizar tareas computacionales de manera simultánea utilizando múltiples unidades de procesamiento. Esto ofrece ventajas significativas en términos de velocidad y eficiencia en la resolución de problemas complejos y demandantes en campos que van desde la investigación científica y la ingeniería hasta el análisis de datos y la inteligencia artificial. La computación en paralelo ha demostrado ser esencial para abordar problemas que serían prácticamente irresolubles utilizando solo una CPU convencional. Ambos códigos funcionan en paralelo utilizando MPI para distribuir la carga de trabajo entre múltiples tareas y calcular una estimación de π utilizando el método de Montecarlo. La tarea 0 en el primer código imprime los resultados finales, incluido el valor estimado de π y el tiempo de ejecución.
</p>

### Codigo calcular_pi hecho por estudiante

```C
# include <stdio.h>
# include <stdlib.h>
# include <time.h>

int main(void){
      int i, count=0, seed=100000;
      double x, y, pi;
      srand(time(NULL));
      for(i=0; i<seed; i++){
            x=(double)rand()/RAND_MAX;

            y=(double)rand()/RAND_MAX;

            double distance=x*x+y*y;

            if(distance <= 1.0){
                count++;
            }
      }
      pi = 4.0*count/seed;
      printf("El valor aproximado de PI es:%lf\n", pi);
      return 0;
}



```

## Codigo Suministrado en clase: Calculate_pi

```C
// This file is part of the HPC workshop 2019 at Durham University
// Author: Christian Arnold
#define _XOPEN_SOURCE
#include <mpi.h>

#include "proto.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* This function draws two random numbers, x and y,
 * from the interval [0, 1) and checks if the corresponding
 * coordinates are within the unit circle, i.e.
 * x^2 + y^2 < 1
 * retuns 1 if true, otherwise 0
 */
int create_and_check_coordinates(void) {
  double x, y;

  x = drand48();
  y = drand48();

  double r_sq = x * x + y * y;

  if (r_sq < 1) /* coordinates are within the circle */
    return 1;

  return 0;
}

/* This function performs N checks and returns
 * the number of coordinate pairs which are within the circle.
 */
int do_checks(int N) {
  int N_in = 0;
  for (int i = 0; i < N; i++)
    N_in += create_and_check_coordinates();

  return N_in;
}

/* This function calculates pi and returns the calculated value given a certain
 * number of random tries */
double calculate_pi(int N) {
  int this_task;
  MPI_Comm_rank(MPI_COMM_WORLD, &this_task);
  /* seed the random number generator */
  srand48(this_task);

  int N_in = do_checks(N);

  int N_in_global;
  int N_global;

  printf("Tarea %i: calculando pi usando %i puntos aleatorios, en %i\n",
         this_task, N, N_in);

  /* now sum up the results across the different tasks */
  MPI_Allreduce(&N_in, &N_in_global, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&N, &N_global, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  double my_pi = 4.0 * N_in_global / N_global;
  return my_pi;
}

```

## Codigo Suministrado en clase: Main

```C

#include "proto.h"
#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef M_PI
#define M_PI 3.141592653589793
#endif
/*
 * The program takes one argument: the name of the file containing the
 * numbers in binary format.
 */
int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);

  // Find out size
  int n_tasks;
  MPI_Comm_size(MPI_COMM_WORLD, &n_tasks);

  // Find out rank
  int this_task;
  MPI_Comm_rank(MPI_COMM_WORLD, &this_task);

  printf("Corriendo %i tareas, tarea número: %i\n", n_tasks, this_task);

  clock_t start, end;

  int N; /* the total count of random coordinates vectors */
  if (argc != 2) {
    if (this_task == 0) {
      fprintf(stderr, "Usa: %s N\n", argv[0]);
      fprintf(stderr, "\nEstimar PI por em método MOntecarlo usando N muestras\n");
    }
    MPI_Finalize();
    return 1;
  }
  N = atoi(argv[1]) / n_tasks;

  start = clock();

  double my_pi = calculate_pi(N);

  end = clock();

  double time = ((double)end - start) / CLOCKS_PER_SEC;

  double error = M_PI - my_pi;

  if (this_task == 0) {
    printf("El valor de pi es: %.20f, El error es:  %.20f\n", my_pi, error);
    printf("El calculo tardó: %g s\n", time);
  }

  MPI_Finalize();
  return 0;
}

```

## Codigo Suministrado en clase: proto

```C
double calculate_pi(int N);

```
