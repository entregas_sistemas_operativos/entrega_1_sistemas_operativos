# Taller 5 (Ensayo y Ejercicios de HackerRank en C)

## La Revolución de la Computación y el Diseño de Sistemas Operativos en Paralelo*

<p style="text-align: justify;">La informática ha experimentado un avance impresionante en las últimas décadas, transformando la forma en que las personas interactúan con las computadoras. Sin embargo, uno de los elementos que ha quedado rezagado en esta evolución es el sistema operativo. A pesar del cambio de las interfaces de línea de comandos a las interfaces gráficas de usuario (GUI) con capacidades de "clic" y "arrastrar", las expectativas de los usuarios promedio han cambiado drásticamente. Este ensayo aborda la revolución de la computación en paralelo y su implementación en este nuevo contexto de usuarios y sus necesidades.</p>

<p style="text-align: justify;">Hace 30 o 40 años, cuando los sistemas operativos estaban en sus primeras etapas de desarrollo, la mayoría de los usuarios de computadoras eran programadores, científicos, ingenieros u otros profesionales que realizaban cálculos intensivos. En ese entonces, la velocidad era fundamental, y los usuarios aceptaban de buena gana bloqueos y reinicios frecuentes. Las computadoras eran herramientas utilizadas principalmente por expertos.Hoy en día, la situación es completamente diferente. Los usuarios modernos esperan que sus sistemas funcionen sin problemas, sin bloqueos ni reinicios. La confiabilidad se ha convertido en la máxima prioridad, y la velocidad de la computadora en sí rara vez es motivo de preocupación. Este cambio en las expectativas ha alterado la relación tradicional entre velocidad y confiabilidad en el ámbito de la informática. A diferencia del pasado, donde la velocidad era el factor más importante, la confiabilidad del sistema operativo es ahora el foco central. Sin embargo, a pesar de esta transformación en las demandas de los usuarios, la confiabilidad de los sistemas operativos sigue siendo insatisfactoria en muchos casos.Por ejemplo, cuando un controlador de dispositivo contiene un error fatal, como escribir en una dirección de memoria no válida o caer en un bucle infinito, los sistemas operativos convencionales tienden a bloquearse por completo. Esto conlleva la pérdida de todas las aplicaciones en ejecución, la eliminación de datos y la interrupción abrupta de transferencias web, FTP y correo electrónico. Esta es la problemática que intentamos abordar.</p>

<p style="text-align: justify;">Dado que los errores de software son inevitables, nuestra estrategia para mejorar la confiabilidad es anticipar fallos y diseñar un sistema operativo autorreparable. Investigaciones han demostrado que el software contiene entre 6 y 16 errores por cada 1000 líneas de código. Por lo tanto, esperar que todo el código sea perfecto resulta poco realista. Hemos concebido MINIX 3 de manera que los errores críticos se aíslen adecuadamente, se detecten defectos y los componentes defectuosos se puedan sustituir sobre la marcha, en muchos casos sin que las aplicaciones se vean afectadas y sin intervención del usuario ni pérdida de datos o trabajo. El enfoque en la computación en paralelo se dirige hacia dispositivos de consumo, como computadoras de escritorio, portátiles y sistemas integrados, excluyendo servidores de alto rendimiento, chips multinúcleo y multiprocesadores. Dentro de esta categoría, tenemos un interés particular en las computadoras utilizadas por usuarios no profesionales y en máquinas de bajo rendimiento, como las que se encuentran en países en desarrollo. La computación en paralelo y el diseño de sistemas operativos autorreparables son esenciales para abordar las cambiantes demandas de los usuarios modernos. La confiabilidad ha superado a la velocidad como principal prioridad, y la capacidad de anticipar fallos y reparar automáticamente son cruciales para cumplir con estas expectativas. Al mirar hacia el futuro, es fundamental considerar las lecciones aprendidas en el pasado y reevaluar ideas previas que aún pueden ser relevantes en el contexto actual. La evolución de la informática continuará, y adaptar los sistemas operativos a estas cambiantes demandas es esencial para el éxito continuo de la tecnología de la información.</p>


## Ejercicios de Hackerrank en C

"Hello World!" in C

![Ejercicio_1](./Imagenes/Ejercicio_1.png)

Ejecucion_1

![Ejecucion_1](./Imagenes/Ejecucion_1.png)

Playing With Characters

![Ejercicio_2](./Imagenes/Ejercicio_2.png)

Ejecucion_2

![Ejecucion_2](./Imagenes/Ejecucion_2.png)

Sum and Difference of Two Numbers

![Ejercicio_3](./Imagenes/Ejercicio_3.png)

Ejecucion_3

![Ejecucion_3](./Imagenes/Ejecucion_3.png)

Functions in C

![Ejercicio_4](./Imagenes/Ejercicio_4.png)

Ejecucion_4

![Ejecucion_4](./Imagenes/Ejecucion_4.png)

Pointers in C

![Ejercicio_5](./Imagenes/Ejercicio_5.png)

Ejecucion_5

![Ejecucion_5](./Imagenes/Ejecucion_5.png)

Conditional Statements in C

``` C
#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* readline();

int main() {
    char* n_endptr;
    char* n_str = readline();
    int n = strtol(n_str, &n_endptr, 10);

    if (n_endptr == n_str || *n_endptr != '\0') { 
        exit(EXIT_FAILURE); 
    }
    char* numbers[] = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    if (n >= 0 && n <= 9) {
        
        printf("%s\n", numbers[n]);
    } else {
        
        printf("Greater than 9\n");
    }
    return 0;
}

char* readline() {
    size_t alloc_length = 1024;
    size_t data_length = 0;
    char* data = malloc(alloc_length);
    while (true) {
        char* cursor = data + data_length;
        char* line = fgets(cursor, alloc_length - data_length, stdin);
        if (!line) { 
            break; 
        }
        data_length += strlen(cursor);
        if (data_length < alloc_length - 1 || data[data_length - 1] == '\n') { 
            break; 
        }
        size_t new_length = alloc_length << 1;
        data = realloc(data, new_length);
        if (!data) { 
            break; 
        }
        alloc_length = new_length;
    }
    if (data[data_length - 1] == '\n') {
        data[data_length - 1] = '\0';
    }
    data = realloc(data, data_length);
    return data;
}

```
Ejecucion_6

![Ejecucion_6](./Imagenes/Ejecucion_6.png)

For Loop in C

``` C
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {
    int a, b;
    scanf("%d%d", &a, &b);
    for (int i = a; i <= b; i++) {
        if (i == 1) {
            printf("one\n");
        } else if (i == 2) {
            printf("two\n");
        } else if (i == 3) {
            printf("three\n");
        } else if (i == 4) {
            printf("four\n");
        } else if (i == 5) {
            printf("five\n");
        } else if (i == 6) {
            printf("six\n");
        } else if (i == 7) {
            printf("seven\n");
        } else if (i == 8) {
            printf("eight\n");
        } else if (i == 9) {
            printf("nine\n");
        } else if (i % 2 == 0) {
            printf("even\n");
        } else {
            printf("odd\n");
        }
    }
    return 0;
}

```

Ejecucion_7

![Ejecucion_7](./Imagenes/Ejecucion_7.png)

Sum of Digits of a Five Digit Number

![Ejercicio_8](./Imagenes/Ejercicio_8.png)

Ejecucion_8

![Ejecucion_8](./Imagenes/Ejecucion_8.png)

Bitwise Operators
``` C
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

void calculate_the_maximum(int n, int k) {
    int max_and = 0;
    int max_or = 0;
    int max_xor = 0;
    for (int a = 1; a <= n; a++) {
        for (int b = a + 1; b <= n; b++) {
            int bitwise_and = a & b;
            int bitwise_or = a | b;
            int bitwise_xor = a ^ b;
            if (bitwise_and > max_and && bitwise_and < k) {
                max_and = bitwise_and;
            }
            if (bitwise_or > max_or && bitwise_or < k) {
                max_or = bitwise_or;
            }
            if (bitwise_xor > max_xor && bitwise_xor < k) {
                max_xor = bitwise_xor;
            }
        }
    }
    printf("%d\n%d\n%d\n", max_and, max_or, max_xor);
}

int main() {
    int n, k;
    scanf("%d %d", &n, &k);
    calculate_the_maximum(n, k);
    return 0; 
}

```

Ejecucion_9

![Ejecucion_9](./Imagenes/Ejecucion_9.png)


Printing Pattern Using Loops

![Ejercicio_10](./Imagenes/Ejercicio_10.png)

Ejecucion_10

![Ejecucion_10](./Imagenes/Ejecucion_10.png)


1D Arrays in C
``` C
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {
    int n;
    scanf("%d", &n);
    int *arr = (int*)malloc(n * sizeof(int));
    if (arr == NULL) {
        printf("Error: No se pudo asignar memoria.\n");
        return 1;
    }
    for (int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }
    int sum = 0;
    for (int i = 0; i < n; i++) {
        sum += arr[i];
    }
    printf("%d\n", sum);
    free(arr);
    return 0;
}

```

Ejecucion_11

![Ejecucion_11](./Imagenes/Ejecucion_11.png)


Array Reversal

``` C
#include <stdio.h>
#include <stdlib.h>

int main() {
    int num, *arr, i;
    scanf("%d", &num);
    arr = (int*) malloc(num * sizeof(int));
    for(i = 0; i < num; i++) {
        scanf("%d", arr + i);
    }
    
    for(i = 0; i < num / 2; i++) {
        int temp = arr[i];
        arr[i] = arr[num - 1 - i];
        arr[num - 1 - i] = temp;
    }
    
    for(i = 0; i < num; i++) {
        printf("%d", *(arr + i));
        if(i < num - 1) {
            printf(" ");
        }
    }
    free(arr); 
    return 0;
}
```
Ejecucion_12 

![Ejecucion_12](./Imagenes/Ejecucion_12.png)

Printing Tokens

![Ejercicio_13](./Imagenes/Ejercicio_13.png)

Ejecucion_13

![Ejecucion_13](./Imagenes/Ejecucion_13.png)

Digit Frequency

![Ejercicio_14](./Imagenes/Ejercicio_14.png)

Ejecucion_14

![Ejecucion_14](./Imagenes/Ejecucion_14.png)

Calculate the Nth term

![Ejercicio_15](./Imagenes/Ejercicio_15.png)

Ejecucion_15

![Ejecucion_15](./Imagenes/Ejecucion_15.png)


Students Marks Sum

``` C
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int marks_summation(int* marks, int number_of_students, char gender) {
  int sum = 0;
    if (gender == 'b') {
        
        for (int i = 0; i < number_of_students; i += 2) {
            sum += marks[i];
        }
    } else if (gender == 'g') {
        for (int i = 1; i < number_of_students; i += 2) {
            sum += marks[i];
        }
    }
    return sum;
}

int main() {
    int number_of_students;
    char gender;
    int sum; 
    scanf("%d", &number_of_students);
    int *marks = (int *) malloc(number_of_students * sizeof (int));
    for (int student = 0; student < number_of_students; student++) {
        scanf("%d", (marks + student));
    }
    scanf(" %c", &gender);
    sum = marks_summation(marks, number_of_students, gender);
    printf("%d", sum);
    free(marks);
    return 0;
}
```

Ejecucion_16

![Ejecucion_16](./Imagenes/Ejecucion_16.png)

Sorting Array of Strings

``` C

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int lexicographic_sort(const char* a, const char* b) {
    return strcmp(a, b);
}
int lexicographic_sort_reverse(const char* a, const char* b) {
    return strcmp(b, a);
}

int count_distinct_characters(const char* str) {
    int char_set[26] = {0};
    int distinct_count = 0;
    for (int i = 0; str[i] != '\0'; i++) {
        if (str[i] >= 'a' && str[i] <= 'z') {
            int index = str[i] - 'a';
            if (char_set[index] == 0) {
                distinct_count++;
                char_set[index] = 1;
            }
        }
    }
    return distinct_count;
}

int sort_by_number_of_distinct_characters(const char* a, const char* b) {
    int distinct_a = count_distinct_characters(a);
    int distinct_b = count_distinct_characters(b);
    if (distinct_a == distinct_b) {
        return lexicographic_sort(a, b);
    }
    return distinct_a - distinct_b;
}

int sort_by_length(const char* a, const char* b) {
    int length_a = strlen(a);
    int length_b = strlen(b);
    if (length_a == length_b) {
        return lexicographic_sort(a, b);
    }
    return length_a - length_b;
}

void string_sort(char** arr,const int len,int (*cmp_func)(const char* a, const char* b)){
     for (int i = 0; i < len - 1; i++) {
        for (int j = 0; j < len - i - 1; j++) {
            if (cmp_func(arr[j], arr[j + 1]) > 0) {
                char* temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}


int main() 
{
    int n;
    scanf("%d", &n);
    char** arr;
	arr = (char**)malloc(n * sizeof(char*));
    for(int i = 0; i < n; i++){
        *(arr + i) = malloc(1024 * sizeof(char));
        scanf("%s", *(arr + i));
        *(arr + i) = realloc(*(arr + i), strlen(*(arr + i)) + 1);
    }
    string_sort(arr, n, lexicographic_sort);
    for(int i = 0; i < n; i++)
        printf("%s\n", arr[i]);
    printf("\n");
    string_sort(arr, n, lexicographic_sort_reverse);
    for(int i = 0; i < n; i++)
        printf("%s\n", arr[i]); 
    printf("\n");
    string_sort(arr, n, sort_by_length);
    for(int i = 0; i < n; i++)
        printf("%s\n", arr[i]);    
    printf("\n");
    string_sort(arr, n, sort_by_number_of_distinct_characters);
    for(int i = 0; i < n; i++)
        printf("%s\n", arr[i]); 
    printf("\n");
}
```

Ejecucion_17

![Ejecucion_17](./Imagenes/Ejecucion_17.png)


Permutations of Strings

``` C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compare(const void *a, const void *b) {
    return strcmp(*(const char **)a, *(const char **)b);
}
int next_permutation(int n, char **s)
{
	int i = n - 2;
    while (i >= 0 && strcmp(s[i], s[i + 1]) >= 0) {
        i--;
    }
    if (i == -1) {
        return 0;
    }
    int j = n - 1;
    while (strcmp(s[j], s[i]) <= 0) {
        j--;
    }
    char *temp = s[i];
    s[i] = s[j];
    s[j] = temp;
    
    for (int k = i + 1, l = n - 1; k < l; k++, l--) {
        temp = s[k];
        s[k] = s[l];
        s[l] = temp;
    }
    return 1;
}

int main()
{
	char **s;
	int n;
	scanf("%d", &n);
	s = calloc(n, sizeof(char*));
	for (int i = 0; i < n; i++)
	{
		s[i] = calloc(11, sizeof(char));
		scanf("%s", s[i]);
	}
	do
	{
		for (int i = 0; i < n; i++)
			printf("%s%c", s[i], i == n - 1 ? '\n' : ' ');
	} while (next_permutation(n, s));
	for (int i = 0; i < n; i++)
		free(s[i]);
	free(s);
	return 0;
}
```

Ejecucion_18

![Ejecucion_18](./Imagenes/Ejecucion_18.png)

Variadic functions in C

``` C
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MIN_ELEMENT 1
#define MAX_ELEMENT 1000000
int  sum (int count,...) {
    int result = 0;
    va_list args;
    va_start(args, count);

    for (int i = 0; i < count; i++) {
        int num = va_arg(args, int);
        result += num;
    }

    va_end(args);
    return result;
}

int min(int count,...) {
    va_list args;
    va_start(args, count);

    int min_value = va_arg(args, int);

    for (int i = 1; i < count; i++) {
        int num = va_arg(args, int);
        if (num < min_value) {
            min_value = num;
        }
    }

    va_end(args);
    return min_value;
}

int max(int count,...) {
    va_list args;
    va_start(args, count);

    int max_value = va_arg(args, int);

    for (int i = 1; i < count; i++) {
        int num = va_arg(args, int);
        if (num > max_value) {
            max_value = num;
        }
    }

    va_end(args);
    return max_value;
}

int test_implementations_by_sending_three_elements() {
    srand(time(NULL));
    
    int elements[3];
    
    elements[0] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[1] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[2] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    
    fprintf(stderr, "Sending following three elements:\n");
    for (int i = 0; i < 3; i++) {
        fprintf(stderr, "%d\n", elements[i]);
    }
    
    int elements_sum = sum(3, elements[0], elements[1], elements[2]);
    int minimum_element = min(3, elements[0], elements[1], elements[2]);
    int maximum_element = max(3, elements[0], elements[1], elements[2]);

    fprintf(stderr, "Your output is:\n");
    fprintf(stderr, "Elements sum is %d\n", elements_sum);
    fprintf(stderr, "Minimum element is %d\n", minimum_element);
    fprintf(stderr, "Maximum element is %d\n\n", maximum_element);
    
    int expected_elements_sum = 0;
    for (int i = 0; i < 3; i++) {
        if (elements[i] < minimum_element) {
            return 0;
        }
        
        if (elements[i] > maximum_element) {
            return 0;
        }
        
        expected_elements_sum += elements[i];
    }
    
    return elements_sum == expected_elements_sum;
}

int test_implementations_by_sending_five_elements() {
    srand(time(NULL));
    
    int elements[5];
    
    elements[0] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[1] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[2] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[3] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[4] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    
    fprintf(stderr, "Sending following five elements:\n");
    for (int i = 0; i < 5; i++) {
        fprintf(stderr, "%d\n", elements[i]);
    }
    
    int elements_sum = sum(5, elements[0], elements[1], elements[2], elements[3], elements[4]);
    int minimum_element = min(5, elements[0], elements[1], elements[2], elements[3], elements[4]);
    int maximum_element = max(5, elements[0], elements[1], elements[2], elements[3], elements[4]);
    
    fprintf(stderr, "Your output is:\n");
    fprintf(stderr, "Elements sum is %d\n", elements_sum);
    fprintf(stderr, "Minimum element is %d\n", minimum_element);
    fprintf(stderr, "Maximum element is %d\n\n", maximum_element);
    
    int expected_elements_sum = 0;
    for (int i = 0; i < 5; i++) {
        if (elements[i] < minimum_element) {
            return 0;
        }
        
        if (elements[i] > maximum_element) {
            return 0;
        }
        
        expected_elements_sum += elements[i];
    }
    
    return elements_sum == expected_elements_sum;
}

int test_implementations_by_sending_ten_elements() {
    srand(time(NULL));
    
    int elements[10];
    
    elements[0] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[1] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[2] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[3] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[4] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[5] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[6] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[7] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[8] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    elements[9] = rand() % (MAX_ELEMENT - MIN_ELEMENT + 1) + MIN_ELEMENT;
    
    fprintf(stderr, "Sending following ten elements:\n");
    for (int i = 0; i < 10; i++) {
        fprintf(stderr, "%d\n", elements[i]);
    }
    
    int elements_sum = sum(10, elements[0], elements[1], elements[2], elements[3], elements[4],
                           elements[5], elements[6], elements[7], elements[8], elements[9]);
    int minimum_element = min(10, elements[0], elements[1], elements[2], elements[3], elements[4],
                           elements[5], elements[6], elements[7], elements[8], elements[9]);
    int maximum_element = max(10, elements[0], elements[1], elements[2], elements[3], elements[4],
                           elements[5], elements[6], elements[7], elements[8], elements[9]);
    
    fprintf(stderr, "Your output is:\n");
    fprintf(stderr, "Elements sum is %d\n", elements_sum);
    fprintf(stderr, "Minimum element is %d\n", minimum_element);
    fprintf(stderr, "Maximum element is %d\n\n", maximum_element);
    
    int expected_elements_sum = 0;
    for (int i = 0; i < 10; i++) {
        if (elements[i] < minimum_element) {
            return 0;
        }
        
        if (elements[i] > maximum_element) {
            return 0;
        }
        
        expected_elements_sum += elements[i];
    }
    
    return elements_sum == expected_elements_sum;
}

int main ()
{
    int number_of_test_cases;
    scanf("%d", &number_of_test_cases);
    
    while (number_of_test_cases--) {
        if (test_implementations_by_sending_three_elements()) {
            printf("Correct Answer\n");
        } else {
            printf("Wrong Answer\n");
        }
        
        if (test_implementations_by_sending_five_elements()) {
            printf("Correct Answer\n");
        } else {
            printf("Wrong Answer\n");
        }
        
        if (test_implementations_by_sending_ten_elements()) {
            printf("Correct Answer\n");
        } else {
            printf("Wrong Answer\n");
        }
    }
    
    return 0;
}
```

Ejecucion_19

![Ejecucion_19](./Imagenes/Ejecucion_19.png)

Boxes through a Tunnel

``` C
#include <stdio.h>
#include <stdlib.h>
#define MAX_HEIGHT 41

struct box
{
    int length;
    int width;
    int height;
};

typedef struct box box;

int get_volume(box b) {
    return b.length * b.width * b.height;
}

int is_lower_than_max_height(box b) {
	return b.height < MAX_HEIGHT ? 1 : 0;
}

int main()
{
	int n;
	scanf("%d", &n);
	box *boxes = malloc(n * sizeof(box));
	for (int i = 0; i < n; i++) {
		scanf("%d%d%d", &boxes[i].length, &boxes[i].width, &boxes[i].height);
	}
	for (int i = 0; i < n; i++) {
		if (is_lower_than_max_height(boxes[i])) {
			printf("%d\n", get_volume(boxes[i]));
		}
	}
	return 0;
}
```

Ejecucion_20

![Ejecucion_20](./Imagenes/Ejecucion_20.png)

Small Triangles, Large Triangles

``` C

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct triangle
{
	int a;
	int b;
	int c;
};

typedef struct triangle triangle;


double calculateArea(int a, int b, int c) {
    double s = (a + b + c) / 2.0;
    return sqrt(s * (s - a) * (s - b) * (s - c));
}

int compare(const void *a, const void *b) {
    double areaA = calculateArea(((triangle*)a)->a, ((triangle*)a)->b, ((triangle*)a)->c);
    double areaB = calculateArea(((triangle*)b)->a, ((triangle*)b)->b, ((triangle*)b)->c);
    if (areaA < areaB) {
        return -1;
    } else if (areaA > areaB) {
        return 1;
    } else {
        return 0;
    }
}

void sort_by_area(triangle* tr, int n) {
    qsort(tr, n, sizeof(triangle), compare);
}


int main()
{
	int n;
	scanf("%d", &n);
	triangle *tr = malloc(n * sizeof(triangle));
	for (int i = 0; i < n; i++) {
		scanf("%d%d%d", &tr[i].a, &tr[i].b, &tr[i].c);
	}
	sort_by_area(tr, n);
	for (int i = 0; i < n; i++) {
		printf("%d %d %d\n", tr[i].a, tr[i].b, tr[i].c);
	}
	return 0;
}
```

Ejecucion_21

![Ejecucion_21](./Imagenes/Ejecucion_21.png)

Post Transition

``` C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STRING_LENGTH 6

struct package
{
    char* id;
    int weight;
};

typedef struct package package;

struct post_office
{
    int min_weight;
    int max_weight;
    package* packages;
    int packages_count;
};

typedef struct post_office post_office;

struct town
{
    char* name;
    post_office* offices;
    int offices_count;
};

typedef struct town town;

void print_all_packages(town t) {
    printf("%s:\n", t.name);
    for (int i = 0; i < t.offices_count; i++) {
        printf("\t%d:\n", i);
        for (int j = 0; j < t.offices[i].packages_count; j++) {
            printf("\t\t%s\n", t.offices[i].packages[j].id);
        }
    }
}

void send_all_acceptable_packages(town* source, int source_office_index, town* target, int target_office_index) {
    post_office* source_office = &source->offices[source_office_index];
    post_office* target_office = &target->offices[target_office_index];

    int num_moved = 0;

    for (int i = 0; i < source_office->packages_count; i++) {
        package* p = &source_office->packages[i];
        if (p->weight >= target_office->min_weight && p->weight <= target_office->max_weight) {
            target_office->packages = realloc(target_office->packages, (target_office->packages_count + 1) * sizeof(package));
            target_office->packages[target_office->packages_count] = *p;
            target_office->packages_count++;
            num_moved++;
        }
    }

    int new_packages_count = source_office->packages_count - num_moved;
    if (new_packages_count > 0) {
        package* new_packages = malloc(new_packages_count * sizeof(package));
        int new_index = 0;
        for (int i = 0; i < source_office->packages_count; i++) {
            package* p = &source_office->packages[i];
            if (p->weight < target_office->min_weight || p->weight > target_office->max_weight) {
                new_packages[new_index] = *p;
                new_index++;
            }
        }
        free(source_office->packages);
        source_office->packages = new_packages;
        source_office->packages_count = new_packages_count;
    } else {
        free(source_office->packages);
        source_office->packages = NULL;
        source_office->packages_count = 0;
    }
}

town town_with_most_packages(town* towns, int towns_count) {
    town most_packages_town = towns[0];

    for (int i = 1; i < towns_count; i++) {
        int total_packages = 0;
        for (int j = 0; j < towns[i].offices_count; j++) {
            total_packages += towns[i].offices[j].packages_count;
        }
        int current_total = 0;
        for (int j = 0; j < most_packages_town.offices_count; j++) {
            current_total += most_packages_town.offices[j].packages_count;
        }
        if (total_packages > current_total) {
            most_packages_town = towns[i];
        }
    }

    return most_packages_town;
}

town* find_town(town* towns, int towns_count, char* name) {
    for (int i = 0; i < towns_count; i++) {
        if (strcmp(towns[i].name, name) == 0) {
            return &towns[i];
        }
    }
    return NULL;
}

int main() {
    int towns_count;
    scanf("%d", &towns_count);
    town* towns = malloc(sizeof(town) * towns_count);
    for (int i = 0; i < towns_count; i++) {
        towns[i].name = malloc(sizeof(char) * MAX_STRING_LENGTH);
        scanf("%s", towns[i].name);
        scanf("%d", &towns[i].offices_count);
        towns[i].offices = malloc(sizeof(post_office) * towns[i].offices_count);
        for (int j = 0; j < towns[i].offices_count; j++) {
            scanf("%d%d%d", &towns[i].offices[j].packages_count, &towns[i].offices[j].min_weight, &towns[i].offices[j].max_weight);
            towns[i].offices[j].packages = malloc(sizeof(package) * towns[i].offices[j].packages_count);
            for (int k = 0; k < towns[i].offices[j].packages_count; k++) {
                towns[i].offices[j].packages[k].id = malloc(sizeof(char) * MAX_STRING_LENGTH);
                scanf("%s", towns[i].offices[j].packages[k].id);
                scanf("%d", &towns[i].offices[j].packages[k].weight);
            }
        }
    }
    int queries;
    scanf("%d", &queries);
    char town_name[MAX_STRING_LENGTH];
    while (queries--) {
        int type;
        scanf("%d", &type);
        switch (type) {
            case 1:
                scanf("%s", town_name);
                town* t = find_town(towns, towns_count, town_name);
                print_all_packages(*t);
                break;
            case 2:
                scanf("%s", town_name);
                town* source = find_town(towns, towns_count, town_name);
                int source_index;
                scanf("%d", &source_index);
                scanf("%s", town_name);
                town* target = find_town(towns, towns_count, town_name);
                int target_index;
                scanf("%d", &target_index);
                send_all_acceptable_packages(source, source_index, target, target_index);
                break;
            case 3:
                printf("Town with the most number of packages is %s\n", town_with_most_packages(towns, towns_count).name);
                break;
        }
    }
    return 0;
}
```

Ejecucion_22

![Ejecucion_22](./Imagenes/Ejecucion_22.png)

Dynamic array in c

``` C
#include <stdio.h>
#include <stdlib.h>

int* total_number_of_books;
int** total_number_of_pages;

int main()
{
    int total_number_of_shelves;
    scanf("%d", &total_number_of_shelves);

    int total_number_of_queries;
    scanf("%d", &total_number_of_queries);

    total_number_of_books = (int *) calloc(sizeof(int), total_number_of_shelves);
    total_number_of_pages = (int **) calloc(sizeof(int *), total_number_of_shelves);

    while (total_number_of_queries--) {
        int type_of_query;
        scanf("%d", &type_of_query);

        if (type_of_query == 1) {
            /*
             * Process the query of first type here.
             */
            int shelf, pages;
            scanf("%d %d", &shelf, &pages);

            total_number_of_books[shelf]++;
            total_number_of_pages[shelf] = realloc(total_number_of_pages[shelf], sizeof(int *) * total_number_of_books[shelf]);
            total_number_of_pages[shelf][total_number_of_books[shelf] - 1] = pages;



        } else if (type_of_query == 2) {
            int x, y;
            scanf("%d %d", &x, &y);
            printf("%d\n", *(*(total_number_of_pages + x) + y));
        } else {
            int x;
            scanf("%d", &x);
            printf("%d\n", *(total_number_of_books + x));
        }
    }

    if (total_number_of_books) {
        free(total_number_of_books);
    }
    
    for (int i = 0; i < total_number_of_shelves; i++) {
        if (*(total_number_of_pages + i)) {
            free(*(total_number_of_pages + i));
        }
    }
    
    if (total_number_of_pages) {
        free(total_number_of_pages);
    }
    
    return 0;
}
```

Ejecucion_23

![Ejecucion_23](./Imagenes/Ejecucion_23.png)


Querying the Document

``` C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<assert.h>
#define MAX_CHARACTERS 1005
#define MAX_PARAGRAPHS 5

char* kth_word_in_mth_sentence_of_nth_paragraph(char**** document, int k, int m, int n) {
    return document[n - 1][m - 1][k - 1];
}

char** kth_sentence_in_mth_paragraph(char**** document, int k, int m) { 
    return document[m - 1][k - 1];
}

char*** kth_paragraph(char**** document, int k) {
    return document[k - 1];
}

char**** get_document(char* text) {
    char ****doc = NULL;
    int i_paragraph = 0;
    int i_sentence = 0;
    int i_word = 0;
    doc = (char ****) malloc(sizeof(char ***));
    doc[0] = (char ***) malloc(sizeof(char **));
    doc[0][0] = (char **) malloc(sizeof(char *));
    char *word = NULL;
    for (char *s = text; *s; ++s)
    {
        if (*s == ' ' || *s == '.')
        {
            fprintf(stderr, "add word p%d s%d w%d: %.*s\n", i_paragraph, i_sentence, i_word, (int)(s - word), word);
            doc[i_paragraph][i_sentence][i_word] = word;
            i_word++;
            doc[i_paragraph][i_sentence] = (char **) realloc(doc[i_paragraph][i_sentence], sizeof(char *) * (i_word + 1));
            if (*s == '.' && s[1] != '\n')
            {
                i_word = 0;
                i_sentence++;
                doc[i_paragraph] = (char ***) realloc(doc[i_paragraph], sizeof(char **) * (i_sentence + 1));
                doc[i_paragraph][i_sentence] = (char **) malloc(sizeof(char *));
            }
            *s = 0;
            word = NULL;
        }
        else if (*s == '\n')
        {
            *s = 0;
            word = NULL;
            i_word = 0;
            i_sentence = 0;
            i_paragraph++;
            doc = (char ****) realloc(doc, sizeof(char ***) * (i_paragraph + 1));
            doc[i_paragraph] = (char ***) malloc(sizeof(char **));
            doc[i_paragraph][0] = (char **) malloc(sizeof(char *));
        }
        else
        {
            if (word == NULL)
            {
                word = s;
                //printf("new word: %s\n", word);
            }
        }
    }
    return doc;
}


char* get_input_text() {	
    int paragraph_count;
    scanf("%d", &paragraph_count);
    char p[MAX_PARAGRAPHS][MAX_CHARACTERS], doc[MAX_CHARACTERS];
    memset(doc, 0, sizeof(doc));
    getchar();
    for (int i = 0; i < paragraph_count; i++) {
        scanf("%[^\n]%*c", p[i]);
        strcat(doc, p[i]);
        if (i != paragraph_count - 1)
            strcat(doc, "\n");
    }
    char* returnDoc = (char*)malloc((strlen (doc)+1) * (sizeof(char)));
    strcpy(returnDoc, doc);
    return returnDoc;
}

void print_word(char* word) {
    printf("%s", word);
}

void print_sentence(char** sentence) {
    int word_count;
    scanf("%d", &word_count);
    for(int i = 0; i < word_count; i++){
        printf("%s", sentence[i]);
        if( i != word_count - 1)
            printf(" ");
    }
} 

void print_paragraph(char*** paragraph) {
    int sentence_count;
    scanf("%d", &sentence_count);
    for (int i = 0; i < sentence_count; i++) {
        print_sentence(*(paragraph + i));
        printf(".");
    }
}

int main() 
{
    char* text = get_input_text();
    char**** document = get_document(text);
    int q;
    scanf("%d", &q);
    while (q--) {
        int type;
        scanf("%d", &type);
        if (type == 3){
            int k, m, n;
            scanf("%d %d %d", &k, &m, &n);
            char* word = kth_word_in_mth_sentence_of_nth_paragraph(document, k, m, n);
            print_word(word);
        }
        else if (type == 2){
            int k, m;
            scanf("%d %d", &k, &m);
            char** sentence = kth_sentence_in_mth_paragraph(document, k, m);
            print_sentence(sentence);
        }
        else{
            int k;
            scanf("%d", &k);
            char*** paragraph = kth_paragraph(document, k);
            print_paragraph(paragraph);
        }
        printf("\n");
    }     
}
```

Ejecucion_24

![Ejecucion_24](./Imagenes/Ejecucion_24.png)


Structuring the Document

``` C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define MAX_CHARACTERS 1005
#define MAX_PARAGRAPHS 5

struct word {
    char* data;
};

struct sentence {
    struct word* data;
    int word_count;//denotes number of words in a sentence
};

struct paragraph {
    struct sentence* data  ;
    int sentence_count;//denotes number of sentences in a paragraph
};

struct document {
    struct paragraph* data;
    int paragraph_count;//denotes number of paragraphs in a document
};
struct document get_document(char* text) {
    struct document doc;
    struct paragraph *cur_paragraph = NULL;
    struct sentence *cur_sentence = NULL;
    char *new_word = NULL;
    doc.data = NULL;
    doc.paragraph_count = 0;
    for (char *s = text; *s; ++s)
    {
        if (*s == ' ' || *s == '.')
        {
            if (cur_paragraph == NULL)
            {
                doc.paragraph_count++;
                doc.data = (struct paragraph *) realloc(doc.data, sizeof(struct paragraph) * doc.paragraph_count);
                cur_paragraph = doc.data + doc.paragraph_count - 1;
                cur_paragraph->data = NULL;
                cur_paragraph->sentence_count = 0;
                cur_sentence = NULL;       
            }
            if (cur_sentence == NULL)
            {
                cur_paragraph->sentence_count++;
                cur_paragraph->data = (struct sentence *) realloc(cur_paragraph->data, sizeof(struct sentence) * cur_paragraph->sentence_count);
                cur_sentence = cur_paragraph->data + cur_paragraph->sentence_count - 1;
                cur_sentence->data = NULL;
                cur_sentence->word_count = 0;
            }
            cur_sentence->word_count++;
            cur_sentence->data = (struct word *) realloc(cur_sentence->data, sizeof(struct word) * cur_sentence->word_count);
            cur_sentence->data[cur_sentence->word_count - 1].data = new_word;
            new_word = NULL;
            if (*s == '.')
                cur_sentence = NULL;        
            *s = 0;
        }
        else if (*s == '\n')
        {
            cur_sentence = NULL;
            cur_paragraph = NULL;
        }
        else
        {
            if (new_word == NULL)
            {
                new_word = s;
            }
        }
    }
    return doc;
}

struct word kth_word_in_mth_sentence_of_nth_paragraph(struct document Doc, int k, int m, int n) {
    return Doc.data[n - 1].data[m - 1].data[k - 1];
}

struct sentence kth_sentence_in_mth_paragraph(struct document Doc, int k, int m) { 
    return Doc.data[m - 1].data[k - 1];
}

struct paragraph kth_paragraph(struct document Doc, int k) {
        return Doc.data[k - 1];
}


void print_word(struct word w) {
    printf("%s", w.data);
}

void print_sentence(struct sentence sen) {
    for(int i = 0; i < sen.word_count; i++) {
        print_word(sen.data[i]);
        if (i != sen.word_count - 1) {
            printf(" ");
        }
    }
}

void print_paragraph(struct paragraph para) {
    for(int i = 0; i < para.sentence_count; i++){
        print_sentence(para.data[i]);
        printf(".");
    }
}

void print_document(struct document doc) {
    for(int i = 0; i < doc.paragraph_count; i++) {
        print_paragraph(doc.data[i]);
        if (i != doc.paragraph_count - 1)
            printf("\n");
    }
}

char* get_input_text() {	
    int paragraph_count;
    scanf("%d", &paragraph_count);
    char p[MAX_PARAGRAPHS][MAX_CHARACTERS], doc[MAX_CHARACTERS];
    memset(doc, 0, sizeof(doc));
    getchar();
    for (int i = 0; i < paragraph_count; i++) {
        scanf("%[^\n]%*c", p[i]);
        strcat(doc, p[i]);
        if (i != paragraph_count - 1)
            strcat(doc, "\n");
    }
    char* returnDoc = (char*)malloc((strlen (doc)+1) * (sizeof(char)));
    strcpy(returnDoc, doc);
    return returnDoc;
}

int main() 
{
    char* text = get_input_text();
    struct document Doc = get_document(text);
    int q;
    scanf("%d", &q);
    while (q--) {
        int type;
        scanf("%d", &type);
        if (type == 3){
            int k, m, n;
            scanf("%d %d %d", &k, &m, &n);
            struct word w = kth_word_in_mth_sentence_of_nth_paragraph(Doc, k, m, n);
            print_word(w);
        }
        else if (type == 2) {
            int k, m;
            scanf("%d %d", &k, &m);
            struct sentence sen= kth_sentence_in_mth_paragraph(Doc, k, m);
            print_sentence(sen);
        }
        else{
            int k;
            scanf("%d", &k);
            struct paragraph para = kth_paragraph(Doc, k);
            print_paragraph(para);
        }
        printf("\n");
    }     
}
```
Ejecucion_25

![Ejecucion_25](./Imagenes/Ejecucion_25.png)
