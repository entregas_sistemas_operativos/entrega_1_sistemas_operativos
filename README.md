# **Entrega taller N° 1**
___
## **Ejercicio N° 1**
Realice un programa que solicite un numero decimal n y permita elegir al usuario una de las tres funciones f1(n), f2(n),f3(n) para operarlo, muestre el resultado en la pantalla.

![Ejercicio numero 1](/Imagenes/Ejercicio_uno.png)

## Codigo de respuesta
```
#include <stdio.h>

float funcion1(float x);
float funcion2(float x);
float funcion3(float x);
    
int main(void){
    float n, func_1, func_2, func_3;
    char opt;
    
    printf("Ingrese una opcion\n");
    scanf("%c", &opt);

    printf("Ingrese un numero decimal\n");
    scanf("%f", &n);
    
    switch(opt){
        case 'A':
            func_1=funcion1(n);
            printf("\n%f es el cuadrado de %f.\n", func_1,n);
            break;
        case 'B':
            func_2=funcion2(n);
            printf("\n%f es n**2 + 1000n de %f.\n", func_2,n);
            break;
        case 'C':
            func_3=funcion3(n);
            printf("\n%f es el caso 3 de %f.\n", func_3,n);
            break;
        default:
            printf("Funcion no valida\n");
            break;
    }
}


float funcion1(float x){
    return x*x;
}

float funcion2(float x){
    return (x*x) + 1000*x;
}

float funcion3(float x){
    float v = 0.0;
    if(x<=0){
        v=10;
    }

    if(x>0 && x<5){
        
        v=(x*x) - x+1;
    }

    if(x>=5){
        v=(2*x) - 1;
    }
    return v;
}
```
___
## **Ejercicio N° 2**

Escriba un programa que permita convertir de grados Fahrenheit (°F)
a Celsius (°C) cuando el usuario ingrese la letra f y la operacion
contraria cuando el usuario ingrese la letra c. Tenga presente las
ecuaciones descritas a continuacion:

![Ejercicio numero 2](/Imagenes/Ejercicio_dos.png)

## Codigo de respuesta
```
#include <stdio.h>

void converCel(void){
    float conv=0.0;
    float valconv;
    printf("Ingrese el valor a convertir: ");
    scanf("%f", &valconv);
    conv=(valconv-32)*(5/9);
    printf("\n%f es la conversion de %f en grados Celcius", valconv, conv);

}

void converFahr(void){
    float conv=0.0;
    float valconv;
    float mixta;
    printf("Ingrese el valor a convertir: ");
    scanf("%f", &valconv);
    mixta=57.6;
    conv=valconv+mixta;
    printf("\n%f es la conversion de %f en grados Fahrenheit", valconv, conv);

}

int main(void){
    int opt;
    printf("Ingrese la tarea que desea realizar: \n");
    printf("1) para convertir de fahrenheit a celcius. \n");
    printf("2) para convertir de celsius a fahrenheit. \n");
    scanf("%d", &opt);
    if(opt==1){
        converCel();
    }
    if(opt==2){
        converFahr();
    }
    return 0;
}
```
___
## **Ejercicio N°3**
Dada la expresión 

![Ejercicio numero 3-a](/Imagenes/Ejercicio_tres_a.png)

donde 

![Ejercicio numero 3-b](/Imagenes/Ejercicio_tres_b.png)

Normalice los datos de salida de los siguientes intervalos:

- [-1,1]
- [1,10]
- [0.5,1]

Para normalizar utilice la siguiente formula:

![Ejercicio numero 3-b](/Imagenes/Ejercicio_tres_c.png)

Donde:

x son los datos a normalizar

xnorm son valores normalizados 
y b conforman el intervalo en el cual se van a normalizar los valores 

si xmax=xmin entonces xnorm=x
a 
```
#include <stdio.h>

float normalize(float x, float xmin, float xmax, float a, float b){
    return a + ((x - xmin) * (b - a)) / (xmax -xmin);
}

int main(){
    int A[20] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};

    //Valores maximo y minimo
    float xmax= 421;
    float xmin= 3;
    //[-1, 1]
    float a1 = -1.0;
    float b1 = 1.0;
    
    //[1, 10]
    float a2 = 1.0;
    float b2 = 10.0;

    //[0.5, 1]
    float a3 = 0.5;
    float b3 = 1.0;
    
    printf("Intervalo [-1, 1]: \n");
    for (int i = 0; i < 20; i++){
        float x_norm = normalize(A[i], xmin, xmax, a1, b1);
        printf("%.2f ", x_norm);
    }

    printf("\n Intervalo [1, 10]: \n");
    for (int i = 0; i < 20; i++){
        float x_norm = normalize(A[i], xmin, xmax, a2, b2);
        printf("%.2f ", x_norm);
    }
    
    printf("\n Intervalo [0.5, 1]: \n");
    for (int i = 0; i < 20; i++){
        float x_norm = normalize(A[i], xmin, xmax, a3, b3);
        printf("%.2f ", x_norm);
    }
    
    return 0;
}
```
___
# **Ejercicio N° 4**
Escriba un programa que le pida al usuario que ingrese el ancho y el largo de una habitación. Una vez leídos los valores, su programa debería calcular y mostrar el área de la habitación. La longitud y el ancho se ingresarán como números de punto floante. Incluya unidades en su mensaje de solicitud y salida; pies o metros.

## Codigo de respuesta
```
#include <stdio.h>

float area_pies(float x, float y);
float area_metros(float x, float y);

int main(void){

    float ancho,largo, uni_pies, uni_metros;
    int optn;
    
    
    printf("En que unidad desea el area: \n");
    printf("1. Pies^2 \n");
    printf("2. Metros^2 \n");
    scanf("%d", &optn);
        
    printf("Ingrese el largo en metros: \n");
    scanf("%f",&largo);

    printf("Ingrese el ancho en metros: \n");
    scanf("%f", &ancho);


    switch(optn){
        case 1:
            uni_pies=area_pies(ancho,largo);
            printf("El tamaño de la habitacion es de: %f Ft^2 \n",uni_pies);
            break;
        case 2:
            uni_metros=area_metros(ancho,largo);
            printf("El tamaño de la habitacion es de: %f M^2 \n",uni_metros);
            break;
    }
}

float area_pies(float x, float y){
    float ft,valor_area;
    ft=10.7639;
    valor_area = (x*y)*ft;
    return valor_area;
}


float area_metros(float x,float y){
    float valor_area;
    valor_area = x*y;
    return valor_area;
}
```
# **Ejercicio N° 5**
Cree un programa que lea la longitud y el ancho del campo de un agricultor del usuario en pies. Mostrar el aréa del campo en acres. 43.560 pies cuadrados en un acre.

## Codigo de respuesta
```
#include <stdio.h>

float area_pies(float x, float y);
float conv_acres(float x, float y);

int main(void){
    float longi, ancho, area_pie, conv_ac;
    int opt;

    printf("\n Elige un procedimiento a realizar: \n");
    printf("\n 1. area en pies cuadrados \n");
    printf("\n 2. convertir un pie cuadrado a acres \n");
    scanf("%d", &opt);

    printf("Ingrese el longitud en metros: \n");
    scanf("%f", &longi);

    printf("Ingrese el ancho en metros: \n");
    scanf("%f", &ancho);

    switch(opt){
        case 1: 
            area_pie=area_pies(longi,ancho);
            printf("El area en pies es de: %f Ft^2 \n", area_pie);
            break;
        case 2:
            conv_ac=conv_acres(longi,ancho);
            printf("Los pies al cuadrado en acres son: %f acres \n", conv_ac);
            break; 
    }

}

float area_pies(float x, float y){
    float ft, valor_area;
    ft=10.7639;
    valor_area = (x*y)*ft;
    return valor_area;
}

float conv_acres(float x, float y){
    float ft, valor_acres;
    ft=10.7639;
    valor_acres=((x*y)*ft)/43560;
    return valor_acres;
}
```
# **Ejercicio N° 6**

![Ejercicio numero 6](/Imagenes/Ejercicio_seis.png)

## Codigo de respuesta
```
#include <stdio.h>

typedef struct {
    double x;
    double y;
    double z;
    double x_1;
    double y_1;
    double z_1;
} Vector;

typedef struct {
    double data[3][3];
} Matrix;

void vectorSubtraction(Vector *result, Vector *v1, Vector *v2) {
    result->x = v1->x - (v2->x)*3;
    result->x_1 = v1->x - v2->x;
    result->y = v1->y - (v2->y)*3;
    result->y_1 = v1->y - v2->y;
    result->z = v1->z - (v2->z)*3;
    result->z_1 = v1->z - v2->z;
}

void matrixVectorMultiplication(Vector *result, Matrix *matrix, Vector *vector) {
    result->x = matrix->data[0][0] * vector->x + matrix->data[0][1] * vector->y + matrix->data[0][2] * vector->z;
    result->y = matrix->data[1][0] * vector->x + matrix->data[1][1] * vector->y + matrix->data[1][2] * vector->z;
    result->z = matrix->data[2][0] * vector->x + matrix->data[2][1] * vector->y + matrix->data[2][2] * vector->z;
}

void matrixScalarMultiplication(Matrix *result, Matrix *matrix, double scalar) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            result->data[i][j] = scalar * matrix->data[i][j];
        }
    }
}

void matrixMatrixMultiplication(Matrix *result, Matrix *matrix1, Matrix *matrix2) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            result->data[i][j] = 0.0;
            for (int k = 0; k < 3; k++) {
                result->data[i][j] += matrix1->data[i][k] * matrix2->data[k][j];
            }
        }
    }
}

int main() {
    // Definir los vectores u y v
    Vector u = {1, 2, 3};
    Vector v = {6, 5, 4};

    // Definir las matrices A y B
    Matrix A = {.data = {{1, 5, 0}, {7, 1, 2}, {0, 0, 1}}};
    Matrix B = {.data = {{-2, 0, 1}, {1, 0, 0}, {4, 1, 0}}};

    // 1. w = u - 3v
    Vector w;
    vectorSubtraction(&w, &u, &v);
    printf("w = (%lf, %lf, %lf)\n", w.x, w.y, w.z);

    // 2. x = u - v
    Vector x;
    vectorSubtraction(&x, &u, &v);
    printf("x = (%lf, %lf, %lf)\n", x.x_1, x.y_1, x.z_1);

    // 3. y = Au
    Vector y;
    matrixVectorMultiplication(&y, &A, &u);
    printf("y = (%lf, %lf, %lf)\n", y.x, y.y, y.z);

    // 4. z = Au - v
    Vector z;
    Vector Au;
    matrixVectorMultiplication(&Au, &A, &u);
    vectorSubtraction(&z, &Au, &v);
    printf("z = (%lf, %lf, %lf)\n", z.x_1, z.y_1, z.z_1);

    // 5. C = 4A - 3B
    Matrix C;
    Matrix tempA, tempB;
    matrixScalarMultiplication(&tempA, &A, 4);
    matrixScalarMultiplication(&tempB, &B, 3);
    matrixMatrixMultiplication(&C, &tempA, &tempB);
    printf("C =\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%lf ", C.data[i][j]);
        }
        printf("\n");
    }

    // 6. D = AB
    Matrix D;
    matrixMatrixMultiplication(&D, &A, &B);
    printf("D =\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            printf("%lf ", D.data[i][j]);
        }
        printf("\n");
    }

    return 0;
}
```
# **Ejercicio N° 7**
Asignar valores a dos variables enteras. Intercambie los valores de almacenados por estas variables utilizando solo punteros a números enteros.

## Codigo de respuesta
```
#include <stdio.h>

void intercambiar(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

int main(){
    int num1 = 5;
    int num2 = 10;

    printf("Antes del intercambio:\n");
    printf("num1 = %d, num2 = %d\n", num1, num2);

    intercambiar(&num1, &num2);

    printf("Después del intercambio:\n");
    printf("num1 = %d, num2 = %d\n", num1, num2);

    return 0;
}
```
# **Ejercicio N° 8**
Escriba un programa que calcule el producto escalar de dos vectores de tipo double de calquier dimensión. Para ello solicite la dimension de los vectores al usuario y luego asigne la memora dinámicamente. Una vez conseguida la memoria se pedirán al usuario los valores de cada elemento de ambos vectores para calcular su producto escalar. Finalmente el programa principal imprimirá por pantalla el resultado y liberará la memoria asignada a los vectores. Tenga en cuenta que:

![Ejercicio numero 8](/Imagenes/Ejercicio_ocho.png)

## Codigo de respuesta
```
#include <stdio.h>
#include <stdlib.h>

int main() {
    int dimension;
    
    printf("Ingrese la dimension de los vectores: ");
    scanf("%d", &dimension);
    
    // Se le Asigna memoria dinámica para los vectores
    double *vector1 = (double *)malloc(dimension * sizeof(double));
    double *vector2 = (double *)malloc(dimension * sizeof(double));
    
    if (vector1 == NULL || vector2 == NULL) {
        printf("Error al asignar memoria dinamica.\n");
        return 1;
    }
    
    // Pedir al usuario los valores para el primer vector
    printf("Ingrese los valores del primer vector:\n");
    for (int i = 0; i < dimension; i++) {
        printf("Elemento %d: ", i + 1);
        scanf("%lf", &vector1[i]);
    }
    
    // Pedir al usuario los valores para el segundo vector
    printf("Ingrese los valores del segundo vector:\n");
    for (int i = 0; i < dimension; i++) {
        printf("Elemento %d: ", i + 1);
        scanf("%lf", &vector2[i]);
    }
    
    // Calcular el producto escalar
    double producto_escalar = 0.0;
    for (int i = 0; i < dimension; i++) {
        producto_escalar += vector1[i] * vector2[i];
    }
    
    // Imprimir el resultado del producto escalar
    printf("El producto escalar de los dos vectores es: %lf\n", producto_escalar);
    
    // Liberar la memoria asignada a los vectores
    free(vector1);
    free(vector2);
    
    return 0;
}
```
# **Ejercicio N° 9**
Escribir una función para calcular la suma y el producto de dos variables x e y. La función debe ser:
```
int funcion(double *suma, double *producto, double *x, double *y);
```
## Codigo de respuesta
```
#include <stdio.h>

int calcularsumaYproducto(double *suma, double *producto, double x, double y){

    *suma= x + y;

    *producto = x * y;

    return 0;
}

int main() {

    double x = 0.0;
    double y = 0.0;
    double sum, producto;
    
    printf("Ingresa dos valores que quieras sumar y multiplicar: \n");
    scanf("%le", &x);
    scanf("%le", &y);

    int result=calcularsumaYproducto(&sum, &producto, x, y);
    
    if (result == 0){
        printf("La suma de %.2f y %.2f es: %.2f \n", x, y, sum);
        printf("El producto de %.2f y %.2f es: %.2f \n", x, y, producto);

    } else{
        printf("Error al calcular la suma y el producto. \n");
    }

    return 0;
}
```
# **Ejercicio N° 10**
Escriba un código para calcular la inversa de una matriz 2x2. Solicite al usuario los elementos de la matriz e imprima los resultados.

## Codigo de respuesta
```
#include <stdio.h>

// Función para calcular la inversa de una matriz 2x2
int inversaMatriz2x2(double matriz[2][2], double inversa[2][2]) {
    double determinante = matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0];

    if (determinante == 0) {
        printf("La matriz no tiene inversa.\n");
        return -1;
    }

    inversa[0][0] = matriz[1][1] / determinante;
    inversa[0][1] = -matriz[0][1] / determinante;
    inversa[1][0] = -matriz[1][0] / determinante;
    inversa[1][1] = matriz[0][0] / determinante;

    return 0;
}

int main() {
    //Definicion de espacio para una matriz 2x2 y su inversa
    double matriz[2][2];
    double inversa[2][2];

    printf("Ingrese los elementos de la matriz 2x2:\n");

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("Elemento [%d][%d]: ", i, j);
            scanf("%lf", &matriz[i][j]);
        }
    }

    int resultado = inversaMatriz2x2(matriz, inversa); 
    //La funcion inversaMatriz2x2 retornara 0 si todo fue un exito
    //lo cual permitira entrar a imprimir los resultados. 

    if (resultado == 0) {
        printf("La matriz ingresada:\n");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                printf("%.2f\t", matriz[i][j]);
            }
            printf("\n");
        }

        printf("\nLa inversa de la matriz:\n");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                printf("%.2f\t", inversa[i][j]);
            }
            printf("\n");
        }
    }

    return 0;
}
```








